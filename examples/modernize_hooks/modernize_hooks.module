<?php

/**
 * @file
 * Contains modernize_hooks.module.
 */

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\modernize_hooks\hooks\ModernizeHooksInfo;
use Drupal\modernize_hooks\hooks\UserAlter;
use Drupal\modernize_hooks\hooks\UserInfo;
use Drupal\modernize_hooks\hooks\UserOperations;

/**
 * Implements hook_help().
 */
function modernize_hooks_help($route_name, RouteMatchInterface $route_match) {
  return \Drupal::classResolver()
    ->getInstanceFromDefinition(ModernizeHooksInfo::class)
    ->help($route_name, $route_match);
}

/**
 * Implements hook_theme().
 */
function modernize_hooks_theme($existing, $type, $theme, $path) {
  return \Drupal::classResolver()
    ->getInstanceFromDefinition(ModernizeHooksInfo::class)
    ->theme($existing, $type, $theme, $path);
}

/**
 * Implements hook_form_user_register_form_alter().
 */
function modernize_hooks_form_user_register_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::classResolver()
    ->getInstanceFromDefinition(UserAlter::class)
    ->userRegisterFormAlter($form, $form_state, $form_id);
}

/**
 * Implements hook_entity_extra_field_info().
 */
function modernize_hooks_entity_extra_field_info() {
  return \Drupal::classResolver()
    ->getInstanceFromDefinition(UserInfo::class)
    ->entityExtraFieldInfo();
}

/**
 * Implements hook_entity_view().
 */
function modernize_hooks_entity_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  \Drupal::classResolver()
    ->getInstanceFromDefinition(UserOperations::class)
    ->entityView($build, $entity, $display, $view_mode);
}
