<?php

function locale_help() {
 ?>
  <P>Normally programs are written and documented in English, and use English to interact with users.  This is true for a great deal of websites.  However, most people are less comfortable with English than with their own native language, and would prefer to use their mother tongue as much as possible.  Many people love see their website showing a lot less of English, and far more of their own language.</P>
  <P>Therefore drupal provides a framework to setup a multi-lingual website, or to overwrite the default texts in English.  We explored the various alternatives to support internationalization and decided to design the framework in such a way that the impact of internationalization on drupal's sources is minimized, modular and that it doesn't require a HTML or PHP wizard to maintain translations.  Maintaining translations had to be simple so it became as easy as filling out forms on the administration page.  A side effect is that translation support adds significant overhead to the dynamic generation of your website.  If you don't need translation support, consider to turning it off from the "conf" section.</P>

  <H3>How to translate texts</H3>

  <P>The actual translation starts at the "overview" of the locale page of the administration pages.  To allow a user to maintain the translations, he obviously needs access to the locale module.  See the account documentation for more information on roles and permissions.</P>
  <P>At the locale page, users with the proper access rights will see the various texts that need translation on the left column of the table.</P>
  <P>Below the text you can see an URI where this text shows up one your site.  Changes are most of these texts will be used and displayed on more than one page, though only one example URI is presented.</P>
  <P>The second column displays the supported languages as defined in the configuration file.  See below for more information on how to support new languages.  If the symbol for a language is seen like <strike>this</strike>, it means that this entry still needs to be translated into that language.  If not, it has been translated already.</P>
  <P>To add or change a translation click the "edit locale" link in the third column, the "operations" column.  You'll be presented the original text and fields for translation in the supported languages.  Enter the translations and confirm by clicking the "Save translations" button.  The translations need not be accurate; they are for your site so you can choose what to show to your users.</P>
  <P>To delete a translation, click the "delete locale" link at the overview page and the translation will be immediately deleted without confirmation.  Deleting translations is convenient for removing texts that belonged to an obsolete module.</P>
  <P>In some texts special strings such as "%a" and "%b" show up.  Those get replaced by some string at run-time when Drupal dynamically generate pages.  You can find out which string this is by looking at the page where the text appears.  This is where the above mentioned URI can come in handy.</P>

  <H3>How to add new languages</H3>

  <P>Adding a new language requires you to edit your configuration file and to edit your SQL database.  Assuming you want to support Dutch (ISO 639 code: "nl") and French (ISO 639 code: "fr"), you add the following line to your configuration file's <CODE>$languages</CODE>-variable:</P>
  <PRE>
    $languages = array("nl" => "Dutch / Nederlands", "fr" => "French / Francais");
  </PRE>
  <P>Note that the default language must come first and that if you want to overwrite the default text you can add an entry for English (ISO 639 code: "en"):</P>
  <PRE>
    $languages = array("en" => "English", "nl" => "Dutch / Nederlands", "fr" => "French / Francais");
  </PRE>
  <P>After having edited your configuration file, make sure your SQL table "locales" has the required database fields setup to host your new translations.  You can add the required rows to your "locales" table from the MySQL prompt:</P>
  <PRE>
    mysql> ALTER TABLE locales ADD en TEXT DEFAULT '' NOT NULL;
    mysql> ALTER TABLE locales ADD nl TEXT DEFAULT '' NOT NULL;
    mysql> ALTER TABLE locales ADD fr TEXT DEFAULT '' NOT NULL;
  </PRE>
 <?php
}

function locale_perm() {
  return array("administer locales");
}

function locale_link($type) {
  if ($type == "admin" && user_access("administer locales")) {
    $links[] = "<a href=\"admin.php?mod=locale\">locales</a>";
  }

  return $links ? $links : array();
}

function locale_conf_options() {
  return form_select("Locale support", "locale", variable_get("locale", 0), array("Disabled", "Enabled"), "Disable locale support if your site does not require translation or internationalization support.");
}

function locale_delete($id) {
  db_query("DELETE FROM locales WHERE id = '$id'");
}

function locale_save($id, $edit) {
  foreach ($edit as $key=>$value) {
    db_query("UPDATE locales SET $key = '". check_input($value) ."' WHERE id = '$id'");
  }
}

function locale_edit($id) {
  global $languages, $REQUEST_URI;
  $result = db_query("SELECT * FROM locales WHERE id = '$id'");
  if ($translation = db_fetch_object($result)) {
    $form .= form_item(t("Original text"), "<PRE>". wordwrap(check_output($translation->string)) ."</PRE>");
    foreach ($languages as $code=>$language) $form .= (strlen($translation->string) > 30) ? form_textarea($language, $code, $translation->$code, 50, 10) : form_textfield($language, $code, $translation->$code, 50, 128);
    $form .= form_hidden("id", $id);
    $form .= form_submit("Save translations");

    return form($REQUEST_URI, $form);
  }
}

function locale_languages($translation) {
  global $languages;

  foreach ($languages as $key=>$value) {
    $output .= ($translation->$key) ? "$key " : "<STRIKE>$key</STRIKE> ";
  }

  return $output;
}

function locale_links($translation) {
  global $languages;

  foreach ($languages as $key=>$value) {
    if ($translation) {
      $output .= "<a href=\"admin.php?mod=locale&op=translated&language=$key\">translated '$key' strings</a> | ";
    }
    else {
      $output .= "<a href=\"admin.php?mod=locale&op=untranslated&language=$key\">untranslated '$key' strings</a> | ";
    }
  }

  return $output;
}

function locale_overview() {
  $result = db_query("SELECT * FROM locales ORDER BY string");

  $output .= "<TABLE BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"2\">\n";
  $output .= " <TR><TH>string</TH><TH>languages</TH><TH COLSPAN=\"2\">operations</TH></TR>\n";
  while ($locale = db_fetch_object($result)) {
    $output .= " <TR><TD>". check_output($locale->string) ."<BR><SMALL><I>". check_output($locale->location) ."</I></SMALL></TD><TD ALIGN=\"center\">". check_output(locale_languages($locale)) ."</TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=edit&id=$locale->id\">edit locale</A></TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=delete&id=$locale->id\">delete locale</A></TD></TR>";
  }
  $output .= "</TABLE>\n";

  return $output;
}

function locale_translated($language) {
  $result = db_query("SELECT * FROM locales WHERE $language != '' ORDER BY string");

  $output .= "<TABLE BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"2\">\n";
  $output .= " <TR><TH>original string</TH><TH>translated string</TH><TH COLSPAN=\"2\">operations</TH></TR>\n";
  while ($locale = db_fetch_object($result)) {
    $output .= " <TR><TD>". check_output($locale->string) ."</TD><TD>". check_output($locale->$language) ."</TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=edit&id=$locale->id\"> edit locale</A></TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=delete&id=$locale->id\">delete locale</A></TD></TR>";
  }
  $output .= "</TABLE>\n";

  return $output;
}

function locale_untranslated($language) {
  $result = db_query("SELECT * FROM locales WHERE $language = '' ORDER BY string");

  $output .= "<TABLE BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"2\">\n";
  $output .= " <TR><TH>string</TH><TH COLSPAN=\"2\">operations</TH></TR>\n";
  while ($locale = db_fetch_object($result)) {
    $output .= " <TR><TD>". check_output($locale->string) ."<BR><SMALL><I>$locale->location</I></SMALL></TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=edit&id=$locale->id\"> edit locale</A></TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=delete&id=$locale->id\">delete locale</A></TD></TR>";
  }
  $output .= "</TABLE>\n";

  return $output;
}

function locale_seek() {
  global $edit, $languages, $REQUEST_URI, $op, $locale_settings;

  if ($edit[id] && session_is_registered("locale_settings")) {
    $edit = $locale_settings;
  }

  if ($op != "search" && is_array($edit)) {
    $locale_settings = $edit;
    session_register("locale_settings");
    if ($edit[status]) {
      switch ($edit[language]) {
        case "all":
          foreach ($languages as $key=>$value) {
            $tmp[] = $key . (check_input($edit[status]) == 1 ? " !=" : " =") ." ''";
          }
          $query[] = implode(" && ", $tmp);
          break;
        case "any":
          foreach ($languages as $key=>$value) {
            $tmp[] = $key . (check_input($edit[status]) == 1 ? " !=" : " =") ." ''";
          }
          $query[] = "(". implode(" || ", $tmp) .")";
          break;
        default:
          $query[] = check_input($edit[language]) . (check_input($edit[status]) == 1 ? " !=" : " =") ." ''";
      }
    }
    if ($edit[module]) $query[] = "location LIKE '%mod=". (check_input($edit[module]) != "all" ? check_input($edit[module]) : "") ."%'";
    if ($edit[string]) $query[] = "string RLIKE '". check_input($edit[string]) ."'";

    $result = db_query("SELECT * FROM locales". (count($query) ? " WHERE ". implode(" && ", $query) : ""));

    $output .= "<TABLE BORDER=\"1\" CELLPADDING=\"2\" CELLSPACING=\"2\">\n";
    $output .= " <TR><TH>string</TH><TH>languages</TH><TH COLSPAN=\"2\">operations</TH></TR>\n";
    while ($locale = db_fetch_object($result)) {
      $output .= " <TR><TD>". check_output($locale->string) ."<BR><SMALL><I>". check_output($locale->location) ."</I></SMALL></TD><TD ALIGN=\"center\">". check_output(locale_languages($locale)) ."</TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=edit&id=$locale->id\">edit locale</A></TD><TD NOWRAP><A HREF=\"admin.php?mod=locale&op=delete&id=$locale->id\">delete locale</A></TD></TR>";
    }
    $output .= "</TABLE>\n";
  }

  $form .= form_select("Language", "language", $edit[language], array_merge(array("all" => "All", "any" => "Any"), $languages));
  $form .= form_select("Status", "status", $edit[status], array("All", "Translated", "Untranslated"));
  $form .= form_select("Module", "module", $edit[module], array_merge(array("0" => "All modules + pages", "all" => "All modules"), module_list()));
  $form .= form_textfield("String", "string", $edit[string], 30, 30, "Leave blank to show all strings. This is treated as a regular expression.");
  $form .= form_submit("Search");

  $output .= form($REQUEST_URI, $form);

  return $output;
}

function locale_admin() {
  global $id, $edit, $op, $language;

  if (!variable_get("locale", 0)) {
    print status("locale disabled.");
  }
  else if (user_access("administer locales")) {
    print "<SMALL>". locale_links(1) . locale_links(0) ."<A HREF=\"admin.php?mod=locale&op=search\">search</A> | <A HREF=\"admin.php?mod=locale&op=overview\">overview</A> | <A HREF=\"admin.php?mod=locale&op=help\">help</A></SMALL><HR>\n";

    switch ($op) {
      case "delete":
        print status(locale_delete(check_input($id)));
        print locale_overview();
        break;
      case "help":
        print locale_help();
        break;
      case "edit":
        print locale_edit(check_input($id));
        break;
      case "translated":
        print locale_translated($language);
        break;
      case "untranslated":
        print locale_untranslated($language);
        break;
      case "overview":
        print locale_overview();
        break;
      case "Save translations":
        print locale_save(check_input($id), $edit);
      default:
        print locale_seek();
    }
  }
  else {
    print message_access();
  }
}

function locale($string) {
  global $locale;
  if (variable_get("locale", 0)) {
    $result = db_query("SELECT id, $locale FROM locales WHERE STRCMP(string, '". addslashes($string) ."') = 0");
    if ($translation = db_fetch_object($result)) $string = ($translation->$locale) ? check_output($translation->$locale) : $string;
    else db_query("INSERT INTO locales (string, location) VALUES ('". addslashes($string) ."', '". check_input(getenv("REQUEST_URI")) ."')");
  }
  return $string;
}

?>