
CREATE TABLE affiliates (
  id int(11)  NOT NULL auto_increment,
  link varchar(255) DEFAULT '' NOT NULL,
  name varchar(255) DEFAULT '' NOT NULL,
  contact varchar(255) DEFAULT '' NOT NULL,
  UNIQUE link (link),
  PRIMARY KEY (id)
);

CREATE TABLE blocks (
  name varchar(64) DEFAULT '' NOT NULL,
  module varchar(64) DEFAULT '' NOT NULL,
  offset tinyint(2) DEFAULT '0' NOT NULL,
  status tinyint(2) DEFAULT '0' NOT NULL,
  weight tinyint(1) DEFAULT '0' NOT NULL,
  region tinyint(1) DEFAULT '0' NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE boxes (
  id tinyint(4)  NOT NULL auto_increment,
  subject varchar(64) DEFAULT '' NOT NULL,
  content text,
  info varchar(128) DEFAULT '' NOT NULL,
  link varchar(128) DEFAULT '' NOT NULL,
  type tinyint(2) DEFAULT '0' NOT NULL,
  UNIQUE subject (subject),
  UNIQUE info (info),
  PRIMARY KEY (id)
);

CREATE TABLE bans (
  id tinyint(4)  NOT NULL auto_increment,
  mask varchar(255) DEFAULT '' NOT NULL,
  type tinyint(2) DEFAULT '0' NOT NULL,
  reason text NOT NULL,
  timestamp int(11),
  UNIQUE mask (mask),
  PRIMARY KEY (id)
);


CREATE TABLE channel (
  id int(11)  NOT NULL auto_increment,
  site varchar(255) DEFAULT '' NOT NULL,
  file varchar(255) DEFAULT '' NOT NULL,
  url varchar(255) DEFAULT '' NOT NULL,
  contact varchar(255) DEFAULT '',
  timestamp int(11),
  UNIQUE site (site),
  UNIQUE file (file),
  UNIQUE url (url),
  PRIMARY KEY (id)
);

CREATE TABLE comments (
  cid int(6)  NOT NULL auto_increment,
  pid int(6) DEFAULT '0' NOT NULL,
  sid int(6) DEFAULT '0' NOT NULL,
  author int(6) DEFAULT '0' NOT NULL,
  subject varchar(64) DEFAULT '' NOT NULL,
  comment text NOT NULL,
  hostname varchar(128) DEFAULT '' NOT NULL,
  timestamp int(11) DEFAULT '0' NOT NULL,
  score int(6) DEFAULT '0' NOT NULL,
  votes int(6) DEFAULT '0' NOT NULL,
  PRIMARY KEY (cid)
);

CREATE TABLE crons (
  module varchar(64) DEFAULT '' NOT NULL,
  scheduled int(11),
  timestamp int(11),
  PRIMARY KEY (module)
);

CREATE TABLE diaries (
  id int(5)  NOT NULL auto_increment,
  author int(6) DEFAULT '0' NOT NULL,
  text text,
  timestamp int(11) DEFAULT '0' NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE drupals (
  id int(11)  NOT NULL auto_increment,
  link varchar(255) DEFAULT '' NOT NULL,
  name varchar(255) DEFAULT '' NOT NULL,
  contact varchar(255) DEFAULT '' NOT NULL,
  UNIQUE link (link),
  PRIMARY KEY (id)
);

CREATE TABLE headlines (
  id int(11) DEFAULT '0' NOT NULL,
  title varchar(255) DEFAULT '' NOT NULL,
  link varchar(255) DEFAULT '' NOT NULL,
  number int(3) DEFAULT '0' NOT NULL,
  UNIQUE title (title),
  UNIQUE link (link)
);

CREATE TABLE layout (
  user int(11) DEFAULT '0' NOT NULL,
  block varchar(64) DEFAULT '' NOT NULL
);

CREATE TABLE modules (
  name varchar(64) DEFAULT '' NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE stories (
  id int(11)  NOT NULL auto_increment,
  author int(6) DEFAULT '0' NOT NULL,
  subject varchar(255) DEFAULT '' NOT NULL,
  abstract text NOT NULL,
  updates text NOT NULL,
  article text NOT NULL,
  category varchar(128) DEFAULT '' NOT NULL,
  department varchar(128) DEFAULT '' NOT NULL,
  timestamp int(11) DEFAULT '0' NOT NULL,
  score int(11) DEFAULT '0' NOT NULL,
  votes int(11) DEFAULT '0' NOT NULL,
  status int(4) DEFAULT '1',
  UNIQUE subject (subject),
  PRIMARY KEY (id)
);

CREATE TABLE users (
  id int(10) unsigned  NOT NULL auto_increment,
  name varchar(60) DEFAULT '' NOT NULL,
  userid varchar(15) DEFAULT '' NOT NULL,
  passwd varchar(20) DEFAULT '' NOT NULL,
  real_email varchar(60) DEFAULT '' NOT NULL,
  fake_email varchar(60) DEFAULT '' NOT NULL,
  url varchar(100) DEFAULT '' NOT NULL,
  stories tinyint(2) DEFAULT '10',
  mode varchar(10) DEFAULT '',
  sort tinyint(1) DEFAULT '0',
  threshold tinyint(1) DEFAULT '0',
  bio tinytext NOT NULL,
  theme varchar(255) DEFAULT '' NOT NULL,
  signature varchar(255) DEFAULT '' NOT NULL,
  last_access int(10) unsigned,
  last_host varchar(255),
  permissions int(10) unsigned DEFAULT '0' NOT NULL,
  status tinyint(4) DEFAULT '0' NOT NULL,
  history text NOT NULL,
  hash varchar(12) DEFAULT '' NOT NULL,
  timezone varchar(8),
  PRIMARY KEY (id)
);

CREATE TABLE watchdog (
  id int(5)  NOT NULL auto_increment,
  level int(2) DEFAULT '0' NOT NULL,
  timestamp int(11) DEFAULT '0' NOT NULL,
  user int(6) DEFAULT '0' NOT NULL,
  message varchar(255) DEFAULT '' NOT NULL,
  location varchar(255) DEFAULT '' NOT NULL,
  hostname varchar(128) DEFAULT '' NOT NULL,
  PRIMARY KEY (id)
);

